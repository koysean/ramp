#!/usr/bin/env python3


#This tests if the periodic self-interactions are summing correctly


# box size
a = 1
cutoff = 10.1*a

element = 'H'
###############

import ase
import amp
from amp.descriptor.gaussian import Gaussian, make_symmetry_functions
from amp.utilities import get_hash
import numpy as np

###################### everything is now scaled so we can get an anyltic result
b = 2*cutoff
cell = [a, b, b]

eta = 1.0 # doesn't matter, we are explicit

def fc(r, cutoff=cutoff):
    val = (1+np.cos(np.pi * r/cutoff))/2
    return np.where(r < cutoff, val , 0)

def G2_func(r, eta=eta, cutoff=cutoff):
    
    G2 = np.exp(-eta*(r/cutoff)**2) 
    return G2 * fc(r, cutoff=cutoff)

G2_total = 0
for i in range(1, int(np.ceil(cutoff/a))):
    r = i*a
    G2_total += 2*G2_func(r) #forwards and backwards

FP_analytic  = G2_total

# with r/rc = 1/3 and eta = 9*np.log(3./2.), the FP value is 0.5 
#FP_analytic  = 0.5
#FPP_analytic = (1.0/cutoff) * (3*np.log(2/3.)-np.pi*np.sqrt(3)/6)
FPP_analytic = 0

print('FP_analytic %f'  % FP_analytic )
print('FPP_analytic %f' % FPP_analytic )
###############

atoms =  ase.Atoms(element,
        positions = [[0,0,0]],
        cell = cell,
        pbc = [True, True, True])


# ----- a pair centered in the cell -----------
#positions = [[a/2-dist/2,0,0], [a/2+dist/2,0,0]]
atoms_centered = atoms.copy()
#atoms_centered.center()


## ----- a pair at the edge of the cell -----------
##positions = [[a-dist/2,0,0], [dist/2,0,0]]
#atoms_edges =  atoms.copy()
#atoms_edges.set_positions( atoms_centered.get_positions() + np.array([a/2, 0, 0]) )
#atoms_edges.wrap()


## ----- a pair with one atom many cells away -----------
##positions = [[a-dist/2,0,0], [dist/2 + 5*a,0,0]]
#positions = atoms_centered.get_positions() 
#positions[1][0] += 5*a 
#atoms_far_im =  atoms.copy()
#atoms_far_im.set_positions(positions)


names = ['Centered Pair']#, 'Pair Across Edge', 'Pair in Different Images']
test_ims = [atoms_centered]#, atoms_edges, atoms_far_im]
##################################
images = {}
hashes = {} 
for i, test_im in enumerate(test_ims): 
    h = get_hash(test_im)
    hashes[names[i]] = h
    images[h] = test_im
    
#############################
elements = [element]

symm_funcs = make_symmetry_functions(elements=elements, type='G2', etas=[eta])
descriptor = Gaussian(Gs=symm_funcs, cutoff=cutoff, fortran = False, dblabel = 'amp')


################### Mikes bits  ############ 
import os 
os.system('rm -r amp-fingerprint-primes.ampdb  amp-fingerprints.ampdb  amp-neighborlists.ampdb')
from amp.descriptor.gaussian import NeighborlistCalculator
my_NLC = NeighborlistCalculator(cutoff)


print()
print('  NEIGHBORLISTS')
for i in range(len(test_ims)):
    print(names[i])
    print('PBC:', test_ims[i].pbc)
    nl = my_NLC.calculate(test_ims[i], hashes[names[i]] )
    for index, _ in  enumerate(nl):
        neighbors, cell_image = _
        print(index, neighbors)
        print( cell_image)
    print()


descriptor.calculate_fingerprints(images, calculate_derivatives=True)

print('  FINGERPRINTS')
for i in range(len(test_ims)):
    print(names[i])
    print('PBC:', test_ims[i].pbc)
    fps = descriptor.fingerprints[ hashes[names[i]] ]
    for fp in fps:
        print(fp)
    print()

print('  FINGERPRINT PRIMES')
for i in range(len(test_ims)):
    print(names[i])
    print('PBC:', test_ims[i].pbc)
    fpps = descriptor.fingerprintprimes[ hashes[names[i]] ]
    for key, value in fpps.items():
        print(key, value)
    print()





